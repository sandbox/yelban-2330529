<?php
/**
 * @file
 * Serves administration pages of CtrlV.
 */

/**
 * Administration page callbacks for the Ctrl+V image upload module.
 */
function ctrlv_path_settings($form, &$form_state) {
  $form['ctrlv_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Upload Subdirectory'),
    '#default_value' => variable_get('ctrlv_path', NULL),
    '#description' => t('Upload files to a subdirectory ( optional ), support node and user tokens, like "ctrl/[user:name]", "PIC[node:type]", etc.'),
  );
  return system_settings_form($form);
}
