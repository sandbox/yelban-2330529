INTRODUCTION
------------
CtrlV is a invisible image upload tool.

When pasting a local image from clipboard to CKEditor,
The module takes the base64 data-encoded URI images
and converts them into normal files when a node is saved.

you can also paste selections from your favorite image editor
such like Photoshop, Snagit, Powerpoint, Word..., too.

It should be the easiest way to upload images to a node.
and make your site more User-Friendly and building a better User Experience.

Works only in Internet Explorer 11+ and Mozilla Firefox browsers.


REQUIREMENTS
------------
This module requires the following modules:
* CkEditor (https://drupal.org/project/ckeditor)
or
* WYSIWYG module with CKEditor (https://drupal.org/project/ckeditor)


CONFIGURATION
-------------
* Configure user permissions in Administration ? Content ? CtrlV imaghe path:
  - Setup the subfolder name for uploaded images (optional)
